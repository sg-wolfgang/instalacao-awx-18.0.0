# Instalação AWX 18.0.0

1. Clone o repositório

```sh
$ git clone -b 18.0.0 https://github.com/ansible/awx.git
```

2. Acesse o diretório clonado com o comando ```cd awx``` e faça o build da imagem:

```sh
$ make docker-compose-build
```

3. Assim que a compilação for concluída, você terá uma ansible/awx_develimagem em seu cache de imagem local. Use o comando ```docker images``` para visualizá-lo da seguinte maneira:

```sh
$ docker images

REPOSITORY                                   TAG                 IMAGE ID            CREATED             SIZE
ansible/awx_devel                            latest              ba9ec3e8df74        26 minutes ago      1.42GB
```

4. Inicie os containers awx, postgress e redis. Com o comando:

```sh
$ make docker-compose
```

> Assim que os contêineres forem iniciados, sua sessão será anexada ao contêiner awx e você poderá assistir as mensagens de registro e eventos em tempo real. Você verá mensagens do Django e do processo de construção do front-end.

> Na primeira vez que você inicia o ambiente, as migrações de banco de dados precisam ser executadas para construir o banco de dados PostgreSQL. Isso levará alguns instantes, mas eventualmente você verá uma saída em sua sessão de terminal semelhante a esta:

```sh
awx_1        | Operations to perform:
awx_1        |   Synchronize unmigrated apps: solo, api, staticfiles, debug_toolbar, messages, channels, django_extensions, ui, rest_framework, polymorphic
awx_1        |   Apply all migrations: sso, taggit, sessions, sites, kombu_transport_django, social_auth, contenttypes, auth, conf, main
awx_1        | Synchronizing apps without migrations:
awx_1        |   Creating tables...
awx_1        |     Running deferred SQL...
awx_1        |   Installing custom SQL...
awx_1        | Running migrations:
awx_1        |   Rendering model states... DONE
awx_1        |   Applying contenttypes.0001_initial... OK
awx_1        |   Applying contenttypes.0002_remove_content_type_name... OK
awx_1        |   Applying auth.0001_initial... OK
...

```

5. O próximo passo é construir a interface do usuário, para isso execute:

```sh
$ docker exec tools_awx_1 make clean-ui ui-devel
```

6. Agora, antes de acessar o AWX você precisa criar um usuário administrador. Para isso, execute o seguinte comando:

```sh
$ docker exec -ti tools_awx_1 awx-manage createsuperuser
```

> Quando perguntado, insira o usuário e senha de sua escolha.

7. Feito isso, o AWX já deverá estar disponível para login na url https://192.168.77.20:8043/#/login sendo possível acessálo utilizando o usuário e senha criados no passo anterior.

> Fonte: https://github.com/ansible/awx/blob/devel/tools/docker-compose/README.md
